#include "decompose.h"

#include <exception>
#include <tuple>
#include <type_traits>
#include <vector>

int main()
{
  { // test void function
    using signature = void();
    using traits = decompose<signature>;
    static_assert(std::is_same_v<traits::signature, signature>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::args, std::tuple<>>);
    static_assert(0 == traits::args_size());
  }

  { // test single argument void function
    using signature = void(double);
    using traits = decompose<signature>;
    static_assert(std::is_same_v<traits::signature, signature>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::arg_type<0>, double>);
    static_assert(std::is_same_v<traits::args, std::tuple<double>>);
    static_assert(1 == traits::args_size());
  }

  { // test five argument char function
    using signature = char(long long, const float&, double*, std::vector<int>, std::true_type&);
    using traits = decompose<signature>;
    static_assert(std::is_same_v<traits::signature, signature>);
    static_assert(std::is_same_v<traits::return_type, char>);
    static_assert(std::is_same_v<traits::arg_type<0>, long long>);
    static_assert(std::is_same_v<traits::arg_type<1>, const float&>);
    static_assert(std::is_same_v<traits::arg_type<2>, double*>);
    static_assert(std::is_same_v<traits::arg_type<3>, std::vector<int>>);
    static_assert(std::is_same_v<traits::arg_type<4>, std::true_type&>);
    static_assert(std::is_same_v<traits::args, std::tuple<long long, const float&, double*, std::vector<int>, std::true_type&>>);
    static_assert(5 == traits::args_size());
  }

  { // test simple lambda
    const auto lambda = []{};
    using traits = decompose<decltype(lambda)>;
    static_assert(std::is_same_v<traits::signature, void()>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::args, std::tuple<>>);
    static_assert(0 == traits::args_size());
  }

  { // test captureless lambda
    int var = 42;
    const auto lambda = [&var](double v) { return static_cast<int>(v) + var; };
    using traits = decompose<decltype(lambda)>;
    static_assert(std::is_same_v<traits::signature, int(double)>);
    static_assert(std::is_same_v<traits::return_type, int>);
    static_assert(std::is_same_v<traits::arg_type<0>, double>);
    static_assert(std::is_same_v<traits::args, std::tuple<double>>);
    static_assert(1 == traits::args_size());
  }

  { // test mutable lambda
    int var = 42;
    const auto lambda = [&var](int i) mutable { return ++var; };
    using traits = decompose<decltype(lambda)>;
    static_assert(std::is_same_v<traits::signature, int(int)>);
    static_assert(std::is_same_v<traits::return_type, int>);
    static_assert(std::is_same_v<traits::arg_type<0>, int>);
    static_assert(std::is_same_v<traits::args, std::tuple<int>>);
    static_assert(1 == traits::args_size());
  }

  { // test throwing lambda
    const auto lambda = []() noexcept(false) { throw std::exception(); };
    using traits = decompose<decltype(lambda)>;
    static_assert(std::is_same_v<traits::signature, void()>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::args, std::tuple<>>);
    static_assert(0 == traits::args_size());
  }

  { // test non-throwing lambda
    const auto lambda = []() noexcept(true) { };
    using traits = decompose<decltype(lambda)>;
    static_assert(std::is_same_v<traits::signature, void()>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::args, std::tuple<>>);
    static_assert(0 == traits::args_size());
  }

  { // test simple function object
    struct FunctionObject
    {
      void operator()() const {}
    };

    using traits = decompose<FunctionObject>;
    static_assert(std::is_same_v<traits::signature, void()>);
    static_assert(std::is_same_v<traits::return_type, void>);
    static_assert(std::is_same_v<traits::args, std::tuple<>>);
    static_assert(0 == traits::args_size());
  }

  { // captureless function object
    struct IsSpace
    {
      bool operator()(char c) const { return c == ' '; }
    };

    using traits = decompose<IsSpace>;
    static_assert(std::is_same_v<traits::signature, bool(char)>);
    static_assert(std::is_same_v<traits::return_type, bool>);
    static_assert(std::is_same_v<traits::arg_type<0>, char>);
    static_assert(std::is_same_v<traits::args, std::tuple<char>>);
    static_assert(1 == traits::args_size());
  }

  { // capturing function object
    struct Add
    {
      Add(int& f) : factor(f) { }
      double operator()(int i) const { return static_cast<double>(i + factor); }
    private:
      int& factor;
    };

    using traits = decompose<Add>;
    static_assert(std::is_same_v<traits::signature, double(int)>);
    static_assert(std::is_same_v<traits::return_type, double>);
    static_assert(std::is_same_v<traits::arg_type<0>, int>);
    static_assert(std::is_same_v<traits::args, std::tuple<int>>);
    static_assert(1 == traits::args_size());
  }

  { // test non-callable type
    // using `decompose` on non callable type compiles, but produces incomplete type
    // can be used in sfinae context to detect non-callable types
    using traits = decompose<bool>;
    //using signature = typename traits::signature; // does not compile
  }
}

