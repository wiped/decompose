Description
=======
Utility used for compile-time querying of any c++17 [`Callable`](http://en.cppreference.com/w/cpp/concept/Callable) properties.  
`decompose` exposes signature, return type, arguments types and size, for given `F` template-parameter.

Usage
=======
Check out `tests.cpp` for basic usage. Compile with `-std=c++17` to run tests.

Compatibility
=======
Compiles under
- gcc 7.0 (-std=c++17 flag)
- clang 4.0 (-std=c++1z flag)

License
=======
Copyright 2017 Mateusz Dudek

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.