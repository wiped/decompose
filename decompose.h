#pragma once

#include <utility>
#include <type_traits>
#include <tuple>

namespace decompose_details
{
template <typename ReturnType, typename... Args>
using traits_gen = typename std::tuple<ReturnType, ReturnType(Args...), std::tuple<Args...>>;

template <typename R, typename... Args>
constexpr auto
decompose_impl(R(*)(Args...))
  -> traits_gen<R, Args...>;

template <typename R, typename F, typename... Args>
constexpr auto
decompose_impl(R(F::*)(Args...))
  -> traits_gen<R, Args...>;

template <typename R, typename F, typename... Args>
constexpr auto
decompose_impl(R(F::*)(Args...)const)
  -> traits_gen<R, Args...>;

template <typename F>
constexpr auto
decompose_impl(F)
  -> decltype(decompose_impl(&F::operator()));

template <typename F>
using decompose_t = decltype(decompose_impl(std::declval<F>()));
}

template <typename F, typename = void>
class decompose;

template <typename F>
class decompose<F, std::void_t<decompose_details::decompose_t<F>>>
{
  using traits = decompose_details::decompose_t<F>;

public:
  using return_type = std::tuple_element_t<0, traits>;
  using signature = std::tuple_element_t<1, traits>;
  using args = std::tuple_element_t<2, traits>;
  static constexpr std::size_t args_size() { return std::tuple_size<args>::value; }

private:
  template <std::size_t I>
  struct arg_at
  {
    static_assert(I < args_size(), "Index out of range.");
    using type = typename std::tuple_element<I, args>::type;
  };

public:
  template <std::size_t I>
  using arg_type = typename arg_at<I>::type;
};

